# Google OR-TOOLS optimization service

Este proyecto utiliza Google OR-Tools. Se trata de una libreria open source para optimización que incluye varias herramientas, en este proyecto particular, se utiliza el solver CP-SAT. 

## Conda Enviroment Setup

El archivo *env.txt* contiene todas las dependencias necesarias para crear el entorno y poder ejecutar el proyecto, en Anaconda Prompt:

```
conda create --name app4 --file env.txt
```

Para que PyCharm utilice este entorno, debe configurarse el interprete de Python de dicho entorno.

En PyCharm: *File > Settings... > Project Settings: <NombreProyecto> > Project Interpreter*

Debe seleccionarse *C:\...\Anaconda3\envs\<nombreEntorno>\python.exe*

## Servicios y ejecución en el servidor

Para correr como servicio una aplicación de Python en un entorno determinado se debe generar un archivo *.bat* en el mismo directorio que la aplicación a ejecutar:

```
@echo on
CALL C:\<path>\Anaconda3\Scripts\activate.bat <nombreEntorno>
python app.py
pause
```

Donde *\<path\>* refiere a la raíz de la instalación de Anaconda y *\<nombreEntorno\>* al entorno a utilizar.

La gestión de servicios puede realizarse con [NSSM](https://nssm.cc/):

```
nssm install <nombreServicio>
```

Abre una UI que permite configurar todos los parametros, mas información en la documentación. (https://nssm.cc/usage)

Los servicios pueden gestionarse vía *services.msc* (Win + R -> services.msc)

## Build & Deploy at INGAR

- Open command prompt as admin an connect to the server by [OPENSSH](http://www.mls-software.com/opensshd.html)
    - ```ssh ingarue@172.16.249.160 -p 22```
    - password: ```avellaneda3657```
- `cd c:\code\app-lab4-py`
- `git pull origin master`
- `net stop ueapp4py` (stop service ueapp4py)
- `copy C:\code\app-lab4-py\*.py C:\apps\app-lab4-py\`
- `copy C:\code\app-lab4-py\start-app-lab4-py.bat C:\apps\app-lab4-py\`
- `del C:\apps\app-lab4-py\app-lab4-py.log`
- `net start ueapp4py` (start service ueapp4py)

### View live logs en powershell 
- `Get-Content -Path "C:\apps\app-lab4-py\app-lab4-py.log" -Wait`



