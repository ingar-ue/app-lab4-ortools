from flask import Flask
from flask import request, Response, jsonify
from flask_cors import CORS
from waitress import serve
from optimization import get_results

app = Flask(__name__)
CORS(app)

@app.route('/optimization', methods=['POST'])
def postHandler():

    if(request.is_json):
        input_data = request.get_json()
        resultdata = get_results(input_data)
        resp = jsonify(resultdata)
        resp.status_code = 200
        return resp
    else:
        return {"status_code": 400}


if __name__ == "__main__":
    serve(app, host='0.0.0.0', port=8087)
    #app.run(host='localhost', port=8083)
