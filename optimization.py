from __future__ import print_function

import collections

# Import Python wrapper for or-tools CP-SAT solver.
from ortools.sat.python import cp_model

def MinimalJobshopSat(data):
    """Minimal jobshop problem."""
    # Create the model.
    model = cp_model.CpModel()

    jobs_data = data['jobs']
    all_jobs = range(len(data['jobsReference']))
    machines_count = 1 + max(int(task[0]) for job in jobs_data for task in job)
    all_machines = range(machines_count)

    # Compute horizon.
    horizon = sum(int(task[1]) for job in jobs_data for task in job)

    task_type = collections.namedtuple('task_type', 'start end interval duration')
    assigned_task_type = collections.namedtuple('assigned_task_type', 'start duration job index')

    # Create jobs.
    all_tasks = {}
    for job in all_jobs:
        for task_id, task in enumerate(jobs_data[job]):
            start_var = model.NewIntVar(0, horizon, 'start_%i_%i' % (job, task_id))
            duration = int(task[1])
            end_var = model.NewIntVar(0, horizon, 'end_%i_%i' % (job, task_id))
            interval_var = model.NewIntervalVar(start_var, duration, end_var, 'interval_%i_%i' % (job, task_id))
            all_tasks[job, task_id] = task_type(
                start=start_var,
                end=end_var,
                interval=interval_var,
                duration=duration)
            print(all_tasks)

    # Create and add disjunctive constraints.
    for machine in all_machines:
        intervals = []
        for job in all_jobs:
            for task_id, task in enumerate(jobs_data[job]):
                if int(task[0]) == machine:
                    intervals.append(all_tasks[job, task_id].interval)
        model.AddNoOverlap(intervals)

    # Add precedence contraints.
    for job in all_jobs:
        for task_id in range(0, len(jobs_data[job]) - 1):
            model.Add(all_tasks[job, task_id + 1].start >= all_tasks[job, task_id].end)

    # Makespan objective.
    obj_var = model.NewIntVar(0, horizon, 'makespan')
    model.AddMaxEquality(
        obj_var,
        [all_tasks[(job, len(jobs_data[job]) - 1)].end for job in all_jobs])
    model.Minimize(obj_var)

    # Solve model.
    solver = cp_model.CpSolver()
    status = solver.Solve(model)

    if status == cp_model.OPTIMAL:
        # Print out makespan.
        print()
        print('Optimal Schedule Length: %i' % solver.ObjectiveValue())
        print()

        # Create one list of assigned tasks per machine.
        assigned_jobs = [[] for _ in all_machines]

        for job in all_jobs:
            for task_id, task in enumerate(jobs_data[job]):
                machine = int(task[0])

                assigned_jobs[machine].append(
                    assigned_task_type(
                        start=solver.Value(all_tasks[job, task_id].start),
                        duration=solver.Value(all_tasks[job, task_id].duration),
                        job=job,
                        index=task_id))

        for machine in all_machines:
            # Sort by starting time.
            assigned_jobs[machine].sort()

        return {"solution": assigned_jobs, "time": solver.ObjectiveValue()}


def get_results(data):
    return MinimalJobshopSat(data)
